FROM python:alpine3.7
COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt
WORKDIR tests
RUN python3 -m pytest
WORKDIR /app
ENTRYPOINT [ "python3" ]
CMD [ "app.py" ]
