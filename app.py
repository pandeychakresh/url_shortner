from flask import Flask, request
from data_manager import DataManager, host, port
data_manager = DataManager()
app = Flask(__name__)

"""
THis Endpoint is to perform shortening operation
over the request from client
"""


@app.route("/short/", methods=['POST'])
def short_long_url():
    data = request.get_json()
    try:
        long_url = data["long_url"]
    except Exception as ex:
        return "Invalid argument or payload, Except Only JSON With Key 'long_url'", 400

    short_url = data_manager.short_url(long_url)
    return short_url, 201


"""
THis is to check that application is running 
"""


@app.route("/check/", methods=['POST', "GET"])
def check():
    return "Working", 200


if __name__ == "__main__":
    app.run(host=host, port=port, debug=True)