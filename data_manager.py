import uuid
import os

"""
Environment variables

"""

host = os.environ.get("host") or "0.0.0.0"
port = os.environ.get("port") or 8082
protocol = os.environ.get("protocol") or "http"


class DataManager:

    """
    This is a class contains utility methods
    to manage the data, manipulate the data,
    and some operations

    """

    def __init__(self, data_file="data.txt"):
        self.__data__ = {}
        self.__data_file__ = None
        self.__initiate_data_file__(data_file)
        self.load_data_from_file()

    def __initiate_data_file__(self, data_file):
        """
        this method is responsible for initialization
         of the data file used in application runtime to
         store and retrieve data.

         :param str data_file: data file name

        """
        self.__data_file__ = open(data_file, "a+")

    def __get_new_short_url_code__(self):

        """
        This method is responsible for generating unique codes
        on every call
        """

        code = uuid.uuid4()
        code = str(code)
        code = code[:8]
        return code

    def load_data_from_file(self):

        """
        This method is responsible for loading the data
        from data file and making it available in the runtime
        environment for fast performance
        """

        self.__data_file__.seek(0)

        while True:
            line = self.__data_file__.readline()
            if not line:
                break
            line = line.strip("\n").split(" ")
            self.__data__[line[0]] = line[1]

    def add_new_url(self, long_url, short_url):
        """
        This method is responsible for storing
        new data in runtime environment and also in
        data file so all data can be managed in a synch.

        :param str long_url: Long url, coming in request
        :param str short_url: Short url, Shorted url for long url
        """

        new_data = "{} {}\n".format(long_url, short_url)
        self.__data_file__.write(new_data)
        self.__data__[long_url] = short_url

    def short_url(self, long_url):
        """
        this method is responsible for shorting
        new long url or return existing shorted url
        for recurring long url.

        :param str long_url: Long url, coming in request

        """

        if long_url in self.__data__:
            return self.__data__[long_url]

        code = self.__get_new_short_url_code__()
        new_url = "{}://{}:{}/{}".format(protocol, host, port, code)
        self.add_new_url(long_url, new_url)

        return new_url



