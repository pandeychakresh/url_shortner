# from data_manager import DataManager
#
# d = DataManager()
# # d.add_new_url("1","2")
# # d.add_new_url("3","4")
# # d.add_new_url("5","6")
# # d.add_new_url("7","8")
# #
# print(d.__data__)
# d.add_new_url("9","0")
# print(d.__data__)
# print(d.__data_file__)


from flask import Flask, request
import os
host = os.environ.get("host")
port = os.environ.get("port")
protocol = os.environ.get("protocol")
app = Flask(__name__)
f = open("data.txt", "a+")



@app.route("/wrt/", methods=["POST"])
def wrt():

    f.write("check\n")
    return "ok"


@app.route("/wrt/close", methods=["POST"])
def cls():
    f.close()
    return "Closed"

@app.route("/wrt/open", methods=["POST"])
def opn():
    f = open("data.txt", "a+")
    return "opened"


if __name__ == "__main__":
    app.run(host=host, port=port, debug=True)