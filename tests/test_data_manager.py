from data_manager import DataManager
data_manager = DataManager()


def test__initiate_data_file__():
    assert data_manager.__data_file__ is not None


def test_load_data_from_file():
    data = {}
    f = open("data.txt")
    while True:
        line = f.readline()
        if not line:
            break
        line = line.strip("\n").split(" ")
        data[line[0]] = line[1]
    f.close()
    assert data == data_manager.__data__


def test_short_url():

    f = open("sample_input.txt")

    while True:
        line = f.readline()
        if not line:
            break
        long_url = line.strip("\n")
        data_manager.short_url(long_url)
        assert data_manager.__data__.get(long_url)

    f = open("sample_output.txt")

    while True:
        line = f.readline()
        if not line:
            break
        line = line.strip("\n").split(" ")
        long_url = line[0]
        short_url = line[1]
        assert short_url == data_manager.short_url(long_url)